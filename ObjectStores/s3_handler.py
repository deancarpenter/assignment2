import ast
import boto3
import botocore
import logging
import os
import sys
import traceback
import time

LOG_FILE_NAME = 'output.log'

REGION = 'us-west-2'

class S3Handler:
    """S3 handler."""

    def __init__(self):
        self.client = boto3.client('s3')
        self.resource = boto3.resource('s3')

        logging.basicConfig(filename=LOG_FILE_NAME,
                            level=logging.DEBUG, filemode='w',
                            format='%(asctime)s %(message)s',
                            datefmt='%m/%d/%Y %I:%M:%S %p')
        self.logger = logging.getLogger("S3Handler")

    def help(self):
        print("Supported Commands:")
        print("1. createdir <bucket_name>")
        print("2. upload <source_file_name> <bucket_name> [<dest_object_name>]")
        print("3. download <dest_object_name> <bucket_name> [<source_file_name>]")
        print("4. delete <dest_object_name> <bucket_name>")
        print("5. deletedir <bucket_name>")
        print("6. find <file_extension> [<bucket_name>] -- e.g.: 1. find txt  2. find txt bucket1 --")
        print("7. listdir [<bucket_name>]")
    
    def _error_messages(self, issue):
        error_message_dict = {}
        error_message_dict['incorrect_parameter_number'] = 'Incorrect number of parameters provided'
        error_message_dict['not_implemented'] = 'Functionality not implemented yet!'
        error_message_dict['bucket_name_exists'] = 'Directory already exists.'
        error_message_dict['bucket_name_empty'] = 'Directory name cannot be empty.'
        error_message_dict['missing_source_file'] = 'Source file cannot be found.'
        error_message_dict['non_existent_bucket'] = 'Directory does not exist.'
        error_message_dict['non_existent_object'] = 'Destination Object does not exist.'
        error_message_dict['unknown_error'] = 'Something was not correct with the request. Try again.'
        error_message_dict['directory_not_empty'] = 'Directory must be empty prior to deleting!'
        error_message_dict['no_files_found'] = 'No files were found!'
        error_message_dict['non_existent_file_name'] = 'File does not exist!'
        error_message_dict['non_existent_directory_name'] = 'Directory does not exist!'

        if issue:
            return error_message_dict[issue]
        else:
            return error_message['unknown_error']

    def _get_file_extension(self, file_name):
        if os.path.exists(file_name):
            return os.path.splitext(file_name)

    def _get(self, bucket_name):
        response = ''
        try:
            response = self.client.head_bucket(Bucket=bucket_name)
        except Exception as e:
            #print(e)
            #traceback.print_exc(file=sys.stdout)
            
            response_code = e.response['Error']['Code']
            if response_code == '404':
                return False
            elif response_code == '403':
                return False
            elif response_code == '200':
                return True
            else:
                raise e
        if response['ResponseMetadata']['HTTPStatusCode'] == 200:
            return True
        else:
            return False

    def createdir(self, bucket_name):
        if not bucket_name:
            return self._error_messages('bucket_name_empty')

        try:
            if self._get(bucket_name):
                return self._error_messages('bucket_name_exists')
            self.client.create_bucket(Bucket=bucket_name,
                                      CreateBucketConfiguration={'LocationConstraint': REGION})
        except Exception as e:
            print(e)
            raise e

        # Success response
        operation_successful = ('Directory %s created.' % bucket_name)
        return operation_successful
        

    def listdir(self, bucket_name):
        # If bucket_name is provided, check that bucket exits.
        # If bucket_name is empty then display the names of all the buckets
        # If bucket_name is provided then display the names of all objects in the bucket

        # List objects
        if bucket_name:
            objs = []
            if self._get(bucket_name):
                bucket = self.resource.Bucket(bucket_name)
                for obj in bucket.objects.all():
                    objs.append(str(obj.key))

                objsString = ",".join(objs)

                return objsString

            else:
                return self._error_messages('non_existent_bucket')

        # No bucket name provided, list all buckets
        else:
            buckets = []
            for bucket in self.resource.buckets.all():
                buckets.append(bucket.name)

            bucketsString = ",".join(buckets)
            return bucketsString

    def upload(self, source_file_name, bucket_name, dest_object_name=''):
        # 1. Parameter Validation
        #    - source_file_name exits in current directory
        #    - bucket_name exists
        # 2. If dest_object_name is not specified then use the source_file_name as dest_object_name
        # 3. SDK call
        #    - When uploading the source_file_name and add it to object's meta-data
        #    - Use self._get_file_extension() method to get the extension of the file.

        if not os.path.exists(source_file_name):
            return self._error_messages('non_existent_file_name')

        if self._get(bucket_name):

            if not dest_object_name:
                dest_object_name = source_file_name

            extension_tuple = self._get_file_extension(source_file_name)
            extension = extension_tuple[1]


            try:
                obj = self.resource.Object(bucket_name, dest_object_name)
                obj.put(Body=open(source_file_name), Metadata={'file_extension': extension})

            except Exception as e:
                print(e)
                raise e

            # Success response
            operation_successful = ('File %s uploaded to bucket %s.' % (source_file_name, bucket_name))
            return operation_successful

        return self._error_messages('non_existent_bucket')


    def download(self, dest_object_name, bucket_name, source_file_name=''):
        # if source_file_name is not specified then use the dest_object_name as the source_file_name
        # If the current directory already contains a file with source_file_name then move it as a backup
        # with following format: <source_file_name.bak.current_time_stamp_in_millis>
        
        # Parameter Validation

        if not source_file_name:
            source_file_name = dest_object_name

        # If this file already exists in CWD, make it a backup by renaming it
        if os.path.exists(source_file_name):
            current_time_stamp_in_millis = int(round(time.time() * 1000))
            os.rename(source_file_name, source_file_name + ".bak." + str(current_time_stamp_in_millis))

        if self._get(bucket_name):
            try:
                # SDK Call
                response = self.resource.Object(bucket_name, dest_object_name).download_file(source_file_name)

            except Exception as e:
                if e.response['Error']['Code'] == "404":
                    return self._error_messages('non_existent_object')

                else:
                    print(e)
                    raise(e)

            # Success response
            operation_successful = ('Object %s downloaded from bucket %s.' % (dest_object_name, bucket_name))
            return operation_successful

        else:
            return self._error_messages('non_existent_bucket')

    def delete(self, dest_object_name, bucket_name):

        if self._get(bucket_name):
            try:
                # SDK Call
                # Check if the object exists..
                self.resource.Object(bucket_name, dest_object_name).load()

                bucket = self.resource.Bucket(bucket_name)
                response = bucket.delete_objects(
                    Delete={
                        'Objects': [
                            {
                                'Key': dest_object_name
                            }
                        ]
                    }
                )

            except Exception as e:
                if e.response['Error']['Code'] == "404":
                    return self._error_messages('non_existent_object')

                else:
                    print(e)
                    raise(e)

        else:
            return self._error_messages('non_existent_bucket')
        
        # Success response
        operation_successful = ('Object %s deleted from bucket %s.' % (dest_object_name, bucket_name))
        return operation_successful

    def deletedir(self, bucket_name):
        # Delete the bucket only if it is empty

        if self._get(bucket_name):
            try:
                bucket = self.resource.Bucket(bucket_name)
                response = bucket.delete()

            except botocore.exceptions.ClientError as e:
                    if e.response['Error']['Code'] == "404":
                        return self._error_messages('non_existent_object')

                    if e.response['Error']['Code'] == 'BucketNotEmpty':
                        return self._error_messages('directory_not_empty')

                    else:
                        print(e)
                        raise(e)

        else:
            return self._error_messages('non_existent_bucket')
        
        # Success response
        operation_successful = ("Deleted bucket %s." % bucket_name)
        return operation_successful

    def find(self, file_extension, bucket_name=''):
        # Return object names that match the given file extension
        # If bucket_name is specified then search for objects in that bucket.
        # If bucket_name is empty then search all buckets

        files_with_extension = []

        if bucket_name:
            if self._get(bucket_name):
                files_with_extension = self.traverse_bucket_for_extension(bucket_name, file_extension, files_with_extension)

                if len(files_with_extension) == 0:
                    return self._error_messages('no_files_found')

                return str(files_with_extension)

            else:
                return self._error_messages('non_existent_bucket')

        # No bucket name provided, iterate through them all like the vikings we are
        else:
            for bucket in self.resource.buckets.all():
                files_with_extension = self.traverse_bucket_for_extension(bucket.name, file_extension, files_with_extension)

            if len(files_with_extension) == 0:
                    return self._error_messages('no_files_found')

            return str(files_with_extension)

    # Traverses a single bucket for an extension. Populates files_with_extension with results
    def traverse_bucket_for_extension(self, bucket_name, file_extension, files_with_extension):
        bucket = self.resource.Bucket(bucket_name)
        bucket_objs_list = list(bucket.objects.all())

        for obj in bucket_objs_list:
            extension = obj.get()['Metadata']['file_extension']
            if extension == file_extension:
                files_with_extension.append(str(obj.key))

        return files_with_extension

    def dispatch(self, command_string):
        parts = command_string.split(" ")
        response = ''

        if parts[0] == 'createdir':
            # Figure out bucket_name from command_string
            if len(parts) > 1:
                bucket_name = parts[1]
                response = self.createdir(bucket_name)
            else:
                # Parameter Validation
                # - Bucket name is not empty
                response = self._error_messages('bucket_name_empty')
        elif parts[0] == 'upload':
            # Figure out parameters from command_string
            # source_file_name and bucket_name are compulsory; dest_object_name is optional
            # Use self._error_messages['incorrect_parameter_number'] if number of parameters is less
            # than number of compulsory parameters

            if len(parts) < 3 or len(parts) > 4:
                response = self._error_messages('incorrect_parameter_number')

            else:
                source_file_name = parts[1]
                bucket_name = parts[2]
                dest_object_name = ''
                if len(parts) == 4:
                    dest_object_name = parts[3]
                response = self.upload(source_file_name, bucket_name, dest_object_name)

        elif parts[0] == 'download':
            # Figure out parameters from command_string
            # dest_object_name and bucket_name are compulsory; source_file_name is optional
            # Use self._error_messages['incorrect_parameter_number'] if number of parameters is less
            # than number of compulsory parameters
            if len(parts) < 3 or len(parts) > 4:
                response = self._error_messages('incorrect_parameter_number')
            else:
                dest_object_name = parts[1]
                bucket_name = parts[2]
                source_file_name = ''
                if len(parts) == 4:
                    source_file_name = parts[3]
            response = self.download(dest_object_name, bucket_name, source_file_name)

        elif parts[0] == 'delete':
            if len(parts) != 3:
                response = self._error_messages('incorrect_parameter_number')
            else:
                dest_object_name = parts[1]
                bucket_name = parts[2]
                response = self.delete(dest_object_name, bucket_name)
        elif parts[0] == 'deletedir':
            if len(parts) != 2:
                response = self._error_messages('incorrect_parameter_number')
            else:
                bucket_name = parts[1]
                response = self.deletedir(bucket_name)
        elif parts[0] == 'find':
            if len(parts) < 2 or len(parts) > 3:
                response = self._error_messages('incorrect_parameter_number')
            else:
                file_extension = parts[1]
                bucket_name = ""
                if(len(parts) == 3):
                    bucket_name = parts[2]
                response = self.find(file_extension, bucket_name)
        elif parts[0] == 'listdir':
            bucket_name = ""
            if len(parts) == 2:
                bucket_name = parts[1]
            response = self.listdir(bucket_name)
        else:
            response = "Command not recognized."
        return response


def main():

    s3_handler = S3Handler()
    
    while True:
        try:
            command_string = ''
            if sys.version_info[0] < 3:
                command_string = raw_input("Enter command ('help' to see all commands, 'exit' to quit)>")
            else:
                command_string = input("Enter command ('help' to see all commands, 'exit' to quit)>")
    
            # Remove multiple whitespaces, if they exist
            command_string = " ".join(command_string.split())
            
            if command_string == 'exit':
                print("Good bye!")
                exit()
            elif command_string == 'help':
                s3_handler.help()
            else:
                response = s3_handler.dispatch(command_string)
                print(response)
        except Exception as e:
            print(e)

if __name__ == '__main__':
    main()
